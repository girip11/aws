# AWS

Getting familiarized with AWS cloud.

## Prerequisites

- AWS personal free tier account.

There are multiple free tier offers in AWS

- Free trial period for a particular service
- 12 month free tier post signup date
- Services that always offer free tier till certain usage capacity. Such free services can be found [here](https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=tier%23always-free&awsf.Free%20Tier%20Categories=*all)

## Resources

- [AWS sample code resources for the udemy course](https://courses.datacumulus.com/downloads/certified-developer-k92/)

---

## References

- [Ultimate AWS Certified Developer](https://www.udemy.com/share/101WgC3@x3RgiRd54eDFJfDC9bXqlJY0jbVpvxK8zp2O7agaSKVMEODC4k-eAq69gG2kskdU/)
