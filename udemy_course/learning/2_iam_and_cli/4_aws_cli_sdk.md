# AWS access

AWS resources can be accessed via

- Management Console
- CLI - Using local terminal or on AWS Cloudshell. Built on AWS Python SDK.
- SDK - programmatic access. Embed with application. Supports Javascript, python, PHP, .NET, Ruby, Java, Go, C++

- Access key ID and secret key are required for access via CLI
  **NOTE**- Never share these credentials to anyone.

## AWS CLI setup

```Bash
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
which aws
aws --version
```

By default, the aws cli gets installed to `/usr/local/bin`

`aws configure` to configure the CLI. We need to enter the access key ID and access secret key to configure the cli access. These credentials are stored at `~/.aws` folder in two files `~/.aws/config` and `~/.aws/credentials`

---

## References

- [Installing or updating the latest version of the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
