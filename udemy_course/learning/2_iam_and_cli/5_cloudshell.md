# Cloudshell

- [Cloud shell region availability](https://docs.aws.amazon.com/general/latest/gr/cloudshell.html)

We can run the commands either on Bash, zhs or powershell. Utilities like Make, pip, sudo, tar, tmux, Vim, Wget and Zip are preinstalled. We can also install any new packages.

This is just an alternate to local terminal. Its free to use upto 1GB storage. Files created in a shell session will be persisted in that free storage.

Files can be uploaded or downloaded.

Terminals can be **tabbed**

---

## References

- [AWS cloudshell faq](https://docs.aws.amazon.com/cloudshell/latest/userguide/faq-list.html#regions-available)
