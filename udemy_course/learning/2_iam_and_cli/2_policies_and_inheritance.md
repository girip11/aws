# Policies and Inheritance

- Users can have inline or inherited policies from the groups that the users are part of.

## Policy structure

- Policy JSON schema version
- Policy ID
- One or more "statements"

Each statement consists of the following

- Sid - statement ID
- Effect - whether to allow or deny
- Principle - account/user/role the policy will be applied to
- Action - actions on the AWS resources
- Resouce - AWS resources these actions and effect apply to.
- Condition - when this policy comes into effect (optional)

## Custom policy

- We can create our own policies.
- We can either author the policy in the JSON or use the visual policy editor on the AWS management console.

## Effective policy

- Once users have inline and inherited policies, we need to know the final effective policy that is applicable to the user.
