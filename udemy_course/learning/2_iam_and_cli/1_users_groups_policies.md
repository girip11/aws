# IAM users, groups and policies

IAM is a global service and doesnt require a region selection.

## Users

- Root user only one. Not recommended.
- Create users, assign groups and only required permissions(policies)
- Users can be directly assigned policies(inline) or can inherit the policies from their respective groups
- One important thing to keep in mind is when users can be part of different groups, and have their own policies, inheritance of policies kicks in.

## Groups

- Users can be grouped.
- Groups can contain only users not other groups
- A user can belong to multiple groups.
- User can also be removed from groups.

## Policies

- Users and groups can be assigned policies(JSON).
- define permissions to users and groups
- Least privilege principle should be followed.

Policy JSON of complete administrative access looks like

```JSON
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
```

## Creating a friendly Console login URL

- By default the account ID is used as the account alias. So the login console url might look like `https://102201345678.signin.aws.amazon.com/console`
- We can edit the `Account Alias` to a human readable name which will form the url `https://<your_account_alias>.signin.aws.amazon.com/console`. This is easier to remember compared to a 12-digit number.
- Using this url we can login as the IAM user.
