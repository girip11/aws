# Multifactor authentication

- We can use supported authenticator apps (virtual MFAs) like Google, Microsoft authenticator.
- Hardware MFA devices are also supported.
- Recommended to set the MFA for root account.

## Password policy

- We can configure the password policy for all the IAM users.
