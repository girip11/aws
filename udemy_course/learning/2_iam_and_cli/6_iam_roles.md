# IAM roles and security tools

- Role can be created targeting AWS services and granting them permissions in the form of policies.
- Role can be created for any AWS service. Commonly used for EC2 functions and lambda services.

## IAM security tools

- IAM credentials report (account level). This report can be downloaded as CSV file.
- IAM access advisor (user-level).
