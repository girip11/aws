# AWS Introduction

## Regions

- AWS datacenter in multiple regions all over the world
- Region is chosen based on

  - data governance and legal requirements
  - latency
  - Services availability in regions(services slowly expand to various regions)
  - pricing in different regions

- Regions are connected to someother regions(its not a mesh connectivity)

## Availability zones

- Each region has multiple availability zones. Usually its 3 AZ per region. These zones have isolated power supply, also redundant networking, connectivity.
- Zones are isolated from each other so that disaster is localized to an availability zone and other zones can still be functioning. So when deploying services with horizontal scaling, we often tend to deploy over multiple availability zones for high availability.
- Availability zones are connected with high bandwidth, ultra low latency networking

## Global services

Global services are available at all edge locations

- Identity and Access management (IAM)
- Route 53 (DNS)
- CloudFront (CDN)
- Web Application Firewall (WAF)

## Region scoped services

- EC2, Beanstalk, Lambda etc

- [Region Table](https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/)
