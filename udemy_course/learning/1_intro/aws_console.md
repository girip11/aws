# AWS Management Console

- Region selection
- Region specific services can be accessed
- Search bar to quickly navigate.
- Service available at a particular region can be found from [this page](https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/)
