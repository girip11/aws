# EC2

- Elastic compute cloud

This service consists of

- Virtual machine (EC2)
- Storing data on virtual drives (EBS)
- Distributing load across machines with load balancer(ELB)
- Scaling the services using auto-scaling group (ASG)

## EC2 configuration options

- Operating System
- CPU, RAM
- storage space
- Network card speed, public IP address
- Security group for firewall
- Bootstrap script - say for automating installing custom tools at first launch. So this script is run exactly once and the script will have root access.

## EC2 instance types

- EC2 has so many instance types with varying CPU, RAM, network and storage (SSD vs EBS) speeds. Ex: t2.micro, c5d.4xlarge etc.

- [EC2 instance types](https://aws.amazon.com/ec2/instance-types/)

### EC2 instance classes

- General purpose
- Compute optimized - compute intensive tasks like batch processing, media transcoding, high performance computing, machine learning etc. Name starts with `C` (C5.large)
- Memory optimized - process large datasets in memory. Name starts with `R`(RAM), `X1`
- Storage optimized - Storage intensive tasks like OLTP systems, databases, in-memory caches, data warehousing solutions. Metric to look at it EBS bandwidth.

### EC2 instance Naming convention

Example m5.2xlarge

- m - belongs to General purpose class of EC2
- 5 - generation of hardware
- 2xlarge - size within the instance class(CPU, RAM)

Useful website to compare EC2 instances is [EC2instances.info](ec2instances.info)
