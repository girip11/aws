# Security groups

- Allow/deny inbound/outbound traffic.
- We can selectively allow access from only certain IP(IPv4 and IPv6) addresses.
- Security groups usually contain rules.
- Additionally Security group can authorize other security groups.(very useful when it comes to load balancers). This is very helpful for enabling communication between EC2 instances and not to be worried about their IP addresses.

## Default setting

- All inbound traffic is blocked and all outbound traffic is allowed by default.

## Attach to EC2 instances

- Instance can have **multiple security groups**.
- A single security group can be attached to different instances.
- Locked down to region/VPC(virtual network) combination.
- Connection timeout could be possibly due to security group issue.
- We can troubleshoot connection issues using reachability analyzer.

## Good practices

- Maintain separate security group for just SSH access.

## SSH troubleshooting

- Connection timeout - Security group issues. Use reachability analyzer
- If its not a security group issues, check the firewall running inside the VM(ex: ufw on ubuntu, windows firewall on windows OS)
- Connection refused - Issue is no SSH service is running on the VM
- Permission denied (publickey,gssapi-keyex,gssapi-with-mic) - wrong user,or security key is wrong or not using the key at all

**NOTE** - EC2 Instance connect **does not work** with all AMI(OS images)

## EC2 roles

- Never enter aws credentials inside VM
- Always use relevant the IAM role feature on EC2.
