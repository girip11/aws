# EC2 Launch instance types

1. On-demand instances - short but uninterrupted workload, predictable pricing. High cost but no upfront payment.
2. Reserved instances for long workloads
   - 75% discount and reservation period of 1-3 years. pay upfront/partial upfront. Recommended for steady-state usage applications.
3. Convertible reserved instances:
   - long workloads with flexible instances. Here we could change the instance type for ex from c5.large to c5.16xlarge. Less discount.
4. Scheduled reserved instances. Still requires 1-3 years of commitment.
5. Spot instance
   - short workloads, cheap but less reliable.
   - upto 90% discount
   - most cost efficient.
   - Use them for workloads that are resilient to failure.(distributed workloads using docker swarm)
6. Dedicated hosts
   - entire physical server and control instance placement.
   - Address compliance requirements or due to server bound software licenses
   - Allocated for 3-year reservation period
7. Dedicated instances
   - Instances running on hardware dedicated to you
   - Other instances belonging to the same account will be hosted in the same hardware.
   - No control over instance placement(automatic)
   - Basically virtual dedicated hosts to run various instances from the same account.
